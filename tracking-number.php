<?php
/*
* Plugin Name: Tracking Number
* Description: List of tracking numbers from B.O.S
* Version: 1.0.3
* Author: Zubli Quzaimer
* Author URI: https://grobox.com.my
* License: B.O.S
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/iliasgrobox/wp-tracking/',
	__FILE__,
	'wp-tracking-bos'
);

$myUpdateChecker->setBranch('main');    

$bos_feed_url = 'https://aa.bosemzi.com/api/tracking_list/v2';

function get_data_json(){
    global $bos_feed_url;
    
    $data_json = get_url($bos_feed_url);
    return $data_json;
}

function get_url($url){
    
    $curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
	curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);

	$res = curl_exec($curl);
	if(curl_error($curl))
	{
		return 'error: '.curl_errno($curl);
	}
	return $res;
}

function get_bos_tracking_number(){
    $data_json = get_data_json();
    
    if(isset($data_json)){
        $data = json_decode($data_json);
        
        if($data == null){
            $html_content = $data_json;
        }
        else {
            $html_content = '';
            $html_content .= "<div class='table-wrapper-scroll my-custom-scrollbar'>";
            $html_content .= "<table class='table table-bordered table-striped'>";
            $html_content .= "<thead><tr>";
            $html_content .= "<th>Pelanggan</th>";
            $html_content .= "</tr></thead>";
            foreach($data as $info){
                $html_content .= "<tr id='tracking'>";
                $receiver_name = isset($info->receiver_name) && !empty($info->receiver_name) ? $info->receiver_name : $info->customer_name;
                $html_content .= "<td>".$receiver_name."<br>";
                $courier_url = isset($info->courier_url) && !empty($info->courier_url) ? $info->courier_url : 'https://track.aftership.com/';
                $html_content .= "<a href='".$courier_url.$info->tracking."' target='_blank'>".$info->tracking."</a></td>";
                $html_content .= "</tr>";
            }
            $html_content .= "</table></div>";
        }
        return $html_content;
    }
}
add_shortcode('bos_tracking_number', 'get_bos_tracking_number');

function bos_tracking_headers()
{
    wp_register_style( 'custom_style', plugins_url( '/custome-style.css', __FILE__ ), array(), '', 'all' );
    wp_enqueue_style( 'custom_style' );
    wp_register_style( 'custom_bts', plugins_url( '/bootstrap.css', __FILE__ ), array(), '', 'all' );
    wp_enqueue_style( 'custom_bts' );
}

#add hook (name of action hook, name of function)
 add_action( 'wp_enqueue_scripts', 'bos_tracking_headers' );